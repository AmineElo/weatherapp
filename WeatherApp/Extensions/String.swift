//
//  String.swift
//  WeatherApp
//
//  Created by Amine Elouattar on 12/3/2023.
//

import Foundation

extension String {
    var firstLetterCapitalized: String {
        let firstLetter = self.prefix(1).capitalized
        let remainingLetters = self.dropFirst().lowercased()
        return firstLetter + remainingLetters
    }
}
