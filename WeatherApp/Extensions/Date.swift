//
//  Date.swift
//  WeatherApp
//
//  Created by Amine Elouattar on 12/3/2023.
//

import Foundation

extension Date {
    var dayOfWeek: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self)
    }
}
