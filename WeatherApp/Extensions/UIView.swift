//
//  UIView.swift
//  WeatherApp
//
//  Created by Amine Elouattar on 12/3/2023.
//

import UIKit

extension UIView {
    func showLoader(){
        let loaderView = UIView()
        
        loaderView.translatesAutoresizingMaskIntoConstraints = false
        loaderView.tag = 9999
        loaderView.backgroundColor = .white
        
        let indocator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
        indocator.translatesAutoresizingMaskIntoConstraints = false
        indocator.startAnimating()
        
        loaderView.addSubview(indocator)
        
        indocator.centerXAnchor.constraint(equalTo: loaderView.centerXAnchor).isActive = true
        indocator.centerYAnchor.constraint(equalTo: loaderView.centerYAnchor).isActive = true
        
        self.addSubview(loaderView)
        
        loaderView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        loaderView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        loaderView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        loaderView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        
        self.bringSubviewToFront(loaderView)
        layoutIfNeeded()
    }
    
    func removeLoader(){
        let view = self.viewWithTag(9999)
        view?.removeFromSuperview()
    }
}
