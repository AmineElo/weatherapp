//
//  CityCollectionViewCell.swift
//  WeatherApp
//
//  Created by Amine Elouattar on 12/3/2023.
//

import UIKit
import WeatherSDK

class CityCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var hourlyForecastCV: UICollectionView!
    @IBOutlet weak var dailyForecastTV: UITableView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var weatherDescLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    
    @IBOutlet weak var hourlyForecastView: UIView!
    @IBOutlet weak var dailyForecastView: UIView!
    
    var city: City? {
        didSet{
            retrieveCityCurrentForecast()
            retrieveHourlyForecast()
        }
    }
    
    var hourlyForecastDataSet = [HourlyForecastViewModel]()
    var dailyForecastDataSet = [DailyForecastViewModel]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.configureHourlyForecastCV()
        self.configureDailyForecastTV()
    }
    
    
    func configureDailyForecastTV(){
        let nibName = UINib(nibName: "DailyForecastTableViewCell", bundle:nil)
        dailyForecastTV.register(nibName, forCellReuseIdentifier: "DailyForecastTableViewCell")
        
        dailyForecastTV.dataSource = self
        dailyForecastTV.delegate = self
        dailyForecastTV.allowsSelection = false
        dailyForecastTV.separatorStyle = .none
    }
    
    func configureHourlyForecastCV(){
        let nibName = UINib(nibName: "HourlyForecastCollectionViewCell", bundle:nil)
        hourlyForecastCV.register(nibName, forCellWithReuseIdentifier: "HourlyForecastCollectionViewCell")
        
        hourlyForecastCV.dataSource = self
        hourlyForecastCV.delegate = self
        hourlyForecastCV.showsHorizontalScrollIndicator = false
    }
    
    func retrieveCityCurrentForecast(){
        guard let city = city else {return}
        WeatherSDK.standard.retrieveCurrentWeatherData(with: city) { currentForecast, error in
            if let currentForecast = currentForecast{
                self.bindCurrentForecastData(currentForecast: currentForecast)
            }
        }
    }
    
    func retrieveHourlyForecast(){
        guard let city = city else {return}
        hourlyForecastView.showLoader()
        dailyForecastTV.showLoader()
        WeatherSDK.standard.retrieveWeatherHourlyForecast(with: city) { hourlyForecast, error in
            self.hourlyForecastView.removeLoader()
            self.dailyForecastTV.removeLoader()
            if let hourlyForecast = hourlyForecast{
                self.hourlyForecastDataSet = self.createHourlyForecastDataSet(from: hourlyForecast)
                self.dailyForecastDataSet = self.createDailyForecastDataSet(from: hourlyForecast)
                
                self.hourlyForecastCV.reloadData()
                self.dailyForecastTV.reloadData()
            }
        }
    }
    
    func bindCurrentForecastData(currentForecast: WeatherForecast){
        nameLabel.text = currentForecast.city.name
        tempLabel.text = "\(currentForecast.currentTemperature)°"
        dayLabel.text = Date().dayOfWeek.firstLetterCapitalized
        
        if let weatherInfo = currentForecast.weather.first {
            weatherDescLabel.text = weatherInfo.description
        }
    }
    
    func createHourlyForecastDataSet(from hourlyForecast: HourlyForecast) -> [HourlyForecastViewModel]{
        return hourlyForecast.list.reduce(into: [HourlyForecastViewModel]()){
            if Calendar.current.date(byAdding: .day, value: 1, to: Date())! > $1.date {
                let hour = Calendar.current.component(.hour, from: $1.date)
                $0.append(HourlyForecastViewModel(
                    hour: (hour > 9) ? "\(hour)" : "0\(hour)",
                    icon: $1.weather.first?.icon ?? "",
                    temp: "\($1.currentTemperature)°"))
            }
        }
    }
    
    func createDailyForecastDataSet(from hourlyForecast: HourlyForecast) -> [DailyForecastViewModel]{
        
        let groupedForecast = hourlyForecast.list.reduce(into:  [[Forecast]]()) { forecastArray, forecast in
            if forecastArray.isEmpty {
                forecastArray.append([forecast])
            }else{
                if let index = forecastArray.firstIndex(where: {$0.contains(where: {$0.date.dayOfWeek == forecast.date.dayOfWeek})}){
                    forecastArray[index].append(forecast)
                }else{
                    forecastArray.append([forecast])
                }
            }
        }
        
        return  groupedForecast.reduce(into: [DailyForecastViewModel]()){
            if $1.count > 0 {
                
                let averageMinTemp = $1.min(by: {$0.minTemparture < $1.minTemparture})!.minTemparture
                let averageMaxTemp = $1.max(by: {$0.maxTemparture < $1.maxTemparture})!.maxTemparture
                
                
                
                $0.append(DailyForecastViewModel(day: $1[0].date.dayOfWeek.firstLetterCapitalized, icon: $1[0].weather.first?.icon ?? "", temp: "\(averageMaxTemp)°  \(averageMinTemp)°"))
                
            }
        }
    }
}

extension CityCollectionViewCell : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int { self.hourlyForecastDataSet.count }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {0}
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HourlyForecastCollectionViewCell", for: indexPath) as! HourlyForecastCollectionViewCell
        
        cell.hourlyForecast = hourlyForecastDataSet[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.hourlyForecastCV.frame.size.width * 1/8, height: self.hourlyForecastCV.frame.size.height);
    }
}

extension CityCollectionViewCell: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { dailyForecastDataSet.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DailyForecastTableViewCell", for: indexPath) as! DailyForecastTableViewCell
        
        cell.item = dailyForecastDataSet[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat { 36.0 }
}
