//
//  DailyForecastTableViewCell.swift
//  WeatherApp
//
//  Created by Amine Elouattar on 12/3/2023.
//

import UIKit

struct DailyForecastViewModel {
    let day: String
    let icon: String
    let temp: String
}

class DailyForecastTableViewCell: UITableViewCell {

    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var tempImageView: UIImageView!
    @IBOutlet weak var tempLabel: UILabel!
    
    var item: DailyForecastViewModel? {
        didSet{
            bindData()
        }
    }
    
    func bindData(){
        guard let item = item else {return}
        
        dayLabel.text = item.day
        tempImageView.image = UIImage(named: item.icon)
        tempLabel.text = item.temp
    }
}
