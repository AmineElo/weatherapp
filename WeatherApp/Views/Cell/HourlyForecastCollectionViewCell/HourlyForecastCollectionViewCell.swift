//
//  HourlyForecastCollectionViewCell.swift
//  WeatherApp
//
//  Created by Amine Elouattar on 12/3/2023.
//

import UIKit
import WeatherSDK

struct HourlyForecastViewModel {
    let hour: String
    let icon: String
    let temp: String
}

class HourlyForecastCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var tempImageView: UIImageView!
    @IBOutlet weak var tempLabel: UILabel!
    
    var hourlyForecast: HourlyForecastViewModel?{
        didSet{
            bindForecastData()
        }
    }

    func bindForecastData(){
        guard let hourlyForecast = hourlyForecast else {return}
        hourLabel.text = hourlyForecast.hour
        tempLabel.text = hourlyForecast.temp
        tempImageView.image = UIImage(named: hourlyForecast.icon)
    }
}
