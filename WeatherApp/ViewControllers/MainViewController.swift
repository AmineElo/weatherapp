//
//  MainViewController.swift
//  WeatherApp
//
//  Created by Amine Elouattar on 11/3/2023.
//

import UIKit
import WeatherSDK
import CoreData

class MainViewController: UIViewController {

    @IBOutlet weak var cityCV: UICollectionView!
    @IBOutlet weak var cityPageControl: UIPageControl!
    @IBOutlet weak var noCityView: UIView!
    
    let coreDataContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var cityDataSet = [City]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        WeatherSDK.standard.setup(appid: "8249e98f674a9ad8aff9a09f99c42634")
        setupViews()
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(managedObjectContextObjectsDidChange), name: NSNotification.Name.NSManagedObjectContextObjectsDidChange, object: self.coreDataContext)
        
    }
    
    private func setupViews(){
        setupCityCollectionView()
        fetchCitiesAndUpdateViews()
    }
    
    func fetchCitiesAndUpdateViews(){
        let cities = fetchSavedCities()
        
        noCityView.isHidden = !cities.isEmpty
        cityCV.isHidden = cities.isEmpty
        
        cityDataSet.append(contentsOf: cities)
        
        cityPageControl.numberOfPages = cities.count
        
        cityCV.reloadData()
    }

    func setupCityCollectionView(){
        cityCV.delegate = self
        cityCV.dataSource = self
        
        cityCV.isPagingEnabled = true
        cityCV.showsHorizontalScrollIndicator = false
        
        let nibName = UINib(nibName: "CityCollectionViewCell", bundle:nil)
        cityCV.register(nibName, forCellWithReuseIdentifier: "CityCollectionViewCell")
    }
    
    func fetchSavedCities() -> [City]{
        let request : NSFetchRequest<CityModel> = CityModel.fetchRequest()
        
        var cities = [CityModel]()
        
        do{
            cities = try coreDataContext.fetch(request)
            cities = cities.sorted(by: {$0.dateAdded! > $1.dateAdded!})
        }
        catch{}
        
        return cities.reduce(into: [City]()){$0.append(City(name: $1.name!, lat: $1.lat, lon: $1.lon, country: $1.country!, state: $1.state!))}
    }
    
    @objc func managedObjectContextObjectsDidChange(notification: Notification){
        cityDataSet.removeAll()
        fetchCitiesAndUpdateViews()
        cityCV.scrollToItem(at: IndexPath(row: 0, section: 0), at: .left, animated: true)
        cityPageControl.currentPage = 0
    }
}

extension MainViewController : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int { self.cityDataSet.count }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {0}
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CityCollectionViewCell", for: indexPath) as! CityCollectionViewCell
        cell.city = cityDataSet[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size;
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        cityPageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
}
