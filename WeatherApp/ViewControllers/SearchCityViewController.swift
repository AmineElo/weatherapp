//
//  SearchCityViewController.swift
//  WeatherApp
//
//  Created by Amine Elouattar on 12/3/2023.
//

import UIKit
import WeatherSDK

class SearchCityViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var suggestionsTV: UITableView!
    
    var cityDataSet = [City]()
    var selectedCityIndex = -1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureSuggestionsTVtTV()
    }
    
    func configureSearchBar(){
        searchBar.delegate = self
    }
    
    func configureSuggestionsTVtTV(){
        suggestionsTV.dataSource = self
        suggestionsTV.delegate = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "cityDetailsSegue", let navigationVC = segue.destination as? UINavigationController, let cityVC = navigationVC.viewControllers.first as? CityForecastViewController {
            cityVC.city = self.cityDataSet[selectedCityIndex]
        }
    }

}

extension SearchCityViewController: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        WeatherSDK.standard.retrieveCitySuggestions(with: searchText, limit: 5) { cities, error in
            self.cityDataSet.removeAll()
            self.cityDataSet.append(contentsOf: cities)
            self.suggestionsTV.reloadData()
        }
    }
}

extension SearchCityViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { cityDataSet.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = "\(cityDataSet[indexPath.row].name), \(cityDataSet[indexPath.row].country)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        selectedCityIndex = indexPath.row
        
        performSegue(withIdentifier: "cityDetailsSegue", sender: self)
    }
}
