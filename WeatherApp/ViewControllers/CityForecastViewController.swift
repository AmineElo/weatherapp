//
//  CityForecastViewController.swift
//  WeatherApp
//
//  Created by Amine Elouattar on 12/3/2023.
//

import UIKit
import WeatherSDK
import CoreData

class CityForecastViewController: UIViewController {
    
    let coreDataContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var city: City?

    override func viewDidLoad() {
        super.viewDidLoad()

        if let nib = Bundle.main.loadNibNamed("CityCollectionViewCell", owner: self),
           let cityDetailsView = nib.first as? CityCollectionViewCell {
            cityDetailsView.frame = view.bounds
            cityDetailsView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            view.addSubview(cityDetailsView)
            
            cityDetailsView.city = self.city
        }
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelTapped))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addTapped))
    }

    @objc func addTapped(){
        guard let city = city else {return}
        saveCity(city){ success in
            if success {
                self.dismiss(animated: true)
            }
        }
    }
    
    @objc func cancelTapped(){
        dismiss(animated: true)
    }
    
    func saveCity(_ city: City, completion: @escaping (Bool)->()){
        let cityModel = CityModel(context: self.coreDataContext)
        
        cityModel.name = city.name
        cityModel.lat = city.lat
        cityModel.lon = city.lon
        cityModel.state = city.state
        cityModel.country = city.country
        cityModel.dateAdded = Date()
        
        do {
            try coreDataContext.save()
            completion(true)
        }
        catch {
            completion(false)
        }
    }
}
